package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
//This application will function as an endpoint that will be used in handling http request.

@RestController
@RequestMapping("/greeting")
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello(){
		return "Hello World";
	}

	//localhost:8080/hi?name=value
	@GetMapping("/hi")
	public String hi(@RequestParam(value="name", defaultValue = "John") String name){
		return String.format("Hi %s", name);
	}

	//Multiple parameters
	//localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value="name", defaultValue = "Joe")String name, @RequestParam(value="friend", defaultValue="jane") String friend){
		return String.format("Hello %s! My name is %s.", friend, name);
	}

	@GetMapping("/hello/{name}")
	public String greetFriend(@PathVariable("name") String name){
		return String.format("Nice to meet you %s", name);
	}



	//ASYNCH
	//Problem 1
	@GetMapping("/welcome/")
	public String welcome(@RequestParam("user") String user, @RequestParam("role") String role) {
		String roles = role;
		switch (roles){
			case "admin": return String.format("Welcome back to the class portal, Admin %s ", user);
			case "teacher": return String.format("Thank you for logging in, Teacher %s ", user);
			case "student": return String.format("Welcome back to the class portal, %s ", user);
			default: return String.format("Role out of range");
		}
	}
	//Problem 2
	private ArrayList<Student> students = new ArrayList<>();
	// In order to retrieve data from the arraylist that each variable is in order. First is to create a Student.java class to hold the given values in the arraylist
	@GetMapping("/register")
	public String register(@RequestParam("id") String id, @RequestParam("name") String name,@RequestParam("course") String course) {
		//As you can see we invoke the Student.java to store values
		Student student = new Student(id, name, course);
		//add
		students.add(student);
		return String.format("%s your id number is registered on the system",id);
	}
	// Problem 3
	// in this problem we need to invoke the values so that we post it in the web through the Student
	public String account(@PathVariable("id") String id) {
		// we use for each so that it's easier to find the student id.
		for (Student student : students) {
			if (student.getId().equals(id)) {
				return String.format("Welcome back %s! You are currently enrolled in s%",student.getName(),student.getCourse());
			}
		}
		return String.format("Your provided  %s is not found in the system!",id);
	}


	//For Friday
	ArrayList<String> enrollees = new ArrayList<String>();

	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "user", defaultValue = "Joe") String user) {
		enrollees.add(user);
		return String.format("Thank you for Enrolling, %s!", user);
	}

	@GetMapping("/getEnrollees")
	public String Enrollees() {
		return String.format(String.valueOf(enrollees));
	}

	@GetMapping("/nameage")
	//localhost:8080/greeting/nameage?name=value&age=value
	public String nameage(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "age", defaultValue = "0") int age) {
		return String.format("Hello %s, My age is %d ", name, age);
	}

	@GetMapping("/courses/{id}")
	// "@PathVariable" allows us to extract data directly from the URL
	public String courses(@PathVariable("id") String id) {
		if(id.equals("java101")){
			return String.format("Java 101, MWF 8:00AM-11:00AM, PHP 3000.00");
		}
		else if (id.equals("sql101")){
			return String.format("SQL 101, TTH 1:00PM-04:00PM, PHP 2000.00");
		}
		else if(id.equals("javaee101")){
			return String.format("Java EE 101, MFW 1:00PM-04:00PM, PHP 3500.00");
		}
		else {
			return String.format("Course cannot be found");
		}
	}


}
